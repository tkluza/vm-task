package pl.vm.library.service.impl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.vm.library.entity.Book;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.book.ReservedBookException;
import pl.vm.library.repository.BookRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.ReservationTo;

import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookServiceImpl.class)
public class BookServiceImplTest {

    @Autowired
    private BookService bookService;

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private BookRepository bookRepository;

    @Mock
    private List<ReservationTo> bookReservationList;

    private Book book;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        book = new Book();
        book.setId(1L);

        Mockito.when(bookRepository.findById(Mockito.anyLong())).thenReturn(Optional.of(book));
    }

    @Test
    public void testDelete_Success() {

        bookService.delete(book.getId());

        Mockito.verify(bookRepository, Mockito.times(1)).delete(book);
    }

    @Test
    public void testDelete_EmptyReservationList_Success() {

        Mockito.when(reservationService.findAllByBookId(book.getId())).thenReturn(bookReservationList);
        Mockito.when(bookReservationList.isEmpty()).thenReturn(true);

        bookService.delete(book.getId());

        Mockito.verify(reservationService, Mockito.times(0)).deleteAll(bookReservationList);
        Mockito.verify(bookRepository, Mockito.times(1)).delete(book);
    }

    @Test
    public void testDelete_NonEmptyReservationList_Success() {

        Mockito.when(reservationService.findAllByBookId(book.getId())).thenReturn(bookReservationList);
        Mockito.when(bookReservationList.isEmpty()).thenReturn(false);

        bookService.delete(book.getId());

        Mockito.verify(reservationService, Mockito.times(1)).deleteAll(bookReservationList);
        Mockito.verify(bookRepository, Mockito.times(1)).delete(book);
    }

    @Test(expected = ReservedBookException.class)
    public void testDelete_NonEmptyReservationList_Fail() {

        Mockito.when(reservationService.isAnyStillValidReservation(Mockito.anyList())).thenReturn(true);

        bookService.delete(book.getId());
    }

    @Test(expected = EntityWithProvidedIdNotFoundException.class)
    public void testDelete_EntityWithProvidedIdNotFoundException() {

        Mockito.when(bookRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(null));

        bookService.delete(Mockito.anyLong());
    }
}
