package pl.vm.library.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vm.library.entity.Book;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.exception.book.ReservedBookException;
import pl.vm.library.repository.BookRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.BookTo;
import pl.vm.library.to.ReservationTo;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReservationService reservationService;

    private ModelMapper mapper = new ModelMapper();
    private Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Override
    public List<BookTo> findAll() {
        List<Book> books = (List<Book>) bookRepository.findAll();

        return books.stream()
            .map(bookEntity -> mapper.map(bookEntity, BookTo.class))
            .collect(Collectors.toList());
    }

    @Override
    public BookTo findByIdAsTo(Long id) {
        Book book = findById(id);
        return mapper.map(book, BookTo.class);
    }

    @Override
    public Book findById(Long id) {
        return bookRepository.findById(id)
            .orElseThrow(EntityWithProvidedIdNotFoundException::new);
    }

    @Override
    public BookTo create(BookTo bookTo) {
        validateNewBook(bookTo);

        Book bookEntity = mapper.map(bookTo, Book.class);

        bookRepository.save(bookEntity);

        return mapper.map(bookEntity, BookTo.class);
    }

    private void validateNewBook(BookTo book) {
        if (book.getId() != null) {
            throw new ParameterValidationException("When creating new Book, the ID should be null.");
        }
    }

    @Override
    @Transactional
    public void delete(Long id) {
        Book book = bookRepository.findById(id)
            .orElseThrow(EntityWithProvidedIdNotFoundException::new);

        List<ReservationTo> bookReservationList = reservationService.findAllByBookId(id);

        validateForDelete(bookReservationList);

        if (CollectionUtils.isNotEmpty(bookReservationList)) {
            logger.info("Reservations for Book with Id={} have been deleted", book.getId());
            this.reservationService.deleteAll(bookReservationList);
        }

        logger.info("Book with Id={} has been deleted", book.getId());
        Book bookEntity = mapper.map(book, Book.class);
        delete(bookEntity);
    }

    private void validateForDelete(List<ReservationTo> bookReservationList) {
        if (reservationService.isAnyStillValidReservation(bookReservationList)) {
            throw new ReservedBookException("Given book is still reserved and can't be deleted");
        }
    }

    private void delete(Book book) {
        bookRepository.delete(book);
    }
}
