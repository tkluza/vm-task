package pl.vm.library.service.impl;

import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vm.library.entity.Book;
import pl.vm.library.entity.Reservation;
import pl.vm.library.entity.User;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.exception.book.ReservedBookException;
import pl.vm.library.exception.reservation.DateValidationException;
import pl.vm.library.repository.ReservationRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.service.ReservationService;
import pl.vm.library.service.UserService;
import pl.vm.library.to.ReservationTo;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationRepository reservationRepository;

    private ModelMapper mapper = new ModelMapper();
    private Logger logger = LoggerFactory.getLogger(ReservationServiceImpl.class);

    @Override
    public List<ReservationTo> findAll() {
        List<Reservation> reservationList = (List<Reservation>) reservationRepository.findAll();

        return reservationList.stream()
            .map(reservation -> mapper.map(reservation, ReservationTo.class))
            .collect(Collectors.toList());
    }

    @Override
    public List<ReservationTo> findAllByBookId(Long bookId) {
        return reservationRepository.findAllByBookId(bookId)
            .stream()
            .map(reservation -> mapper.map(reservation, ReservationTo.class))
            .collect(Collectors.toList());
    }

    @Override
    public ReservationTo findById(Long id) {
        return reservationRepository.findById(id)
            .map(reservation -> mapper.map(reservation, ReservationTo.class))
            .orElseThrow(EntityWithProvidedIdNotFoundException::new);
    }

    @Override
    public ReservationTo create(ReservationTo reservation) {
        User user = this.userService.findById(reservation.getUserId());
        Book book = this.bookService.findById(reservation.getBookId());

        validateNewReservation(reservation);

        logger.debug("Create new reservation");

        Reservation reservationEntity = mapper.map(reservation, Reservation.class);
        reservationEntity.setUser(user);
        reservationEntity.setBook(book);

        reservationRepository.save(reservationEntity);

        return mapper.map(reservationEntity, ReservationTo.class);
    }

    private void validateNewReservation(ReservationTo reservation) {
        if (reservation.getId() != null) {
            throw new ParameterValidationException("When creating new Reservation, the ID should be null.");
        }

        List<ReservationTo> bookReservationList = findAllByBookId(reservation.getBookId());
        if (isAnyStillValidReservation(bookReservationList)) {
            throw new ReservedBookException("Given Book has been already reserved.");
        }
    }

    @Override
    public void deleteAll(List<ReservationTo> reservationList) {
        List<Reservation> reservations = reservationList
            .stream()
            .map(reservation -> mapper.map(reservation, Reservation.class))
            .collect(Collectors.toList());

        this.reservationRepository.deleteAll(reservations);
    }

    @Override
    public ReservationTo updateDateTo(Long id, Date toDate) {
        Reservation reservation = reservationRepository.findById(id)
            .orElseThrow(EntityWithProvidedIdNotFoundException::new);

        logger.debug("Update DateTo for Reservation with Id={}", id);

        validateToDateUpdate(toDate, reservation);

        reservation.setToDate(toDate);

        return mapper.map(reservation, ReservationTo.class);
    }

    private void validateToDateUpdate(Date toDate, Reservation reservation) {
        if (toDate.before(reservation.getToDate())) {
            throw new DateValidationException("When a Reservation is extended, new date cannot be before current expiration date.");
        }
    }

    public boolean isAnyStillValidReservation(List<ReservationTo> reservationList) {
        boolean stillValid = false;

        if (CollectionUtils.isNotEmpty(reservationList)) {
            Date today = Calendar.getInstance().getTime();
            stillValid = reservationList.stream()
                .anyMatch(reservation -> reservation.getToDate().after(today));
        }

        return stillValid;
    }
}
