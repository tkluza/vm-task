package pl.vm.library.service;

import pl.vm.library.to.ReservationTo;

import java.util.Date;
import java.util.List;

/**
 * The Service which contains business logic for Reservation.
 */
public interface ReservationService {

    /**
     * Returns all Reservations
     *
     * @return all Reservations
     */
    public List<ReservationTo> findAll();

    /**
     * Returns all Reservations for given BookId
     *
     * @param bookId BookId
     * @return all Reservations for given BookId
     */
    public List<ReservationTo> findAllByBookId(Long bookId);

    /**
     * Returns the Reservation with the given ID.
     *
     * @return the found Reservation
     */
    public ReservationTo findById(Long id);

    /**
     * Creates new Reservation for given Transfer Object
     *
     * @param reservation to be created
     * @return the persisted Reservation
     */
    public ReservationTo create(ReservationTo reservation);

    /**
     * Deletes all given Reservations
     *
     * @param reservationList Reservations to delete
     */
    public void deleteAll(List<ReservationTo> reservationList);

    /**
     * Updates toDate for a given Reservation
     *
     * @param id     of Reservation to be updated
     * @param toDate new toDate
     * @return ReservationTo with updated toDate
     */
    public ReservationTo updateDateTo(Long id, Date toDate);

    /**
     * Checks, if given book can be reserved
     *
     * @param book to check availability
     * @return book's availability
     */
    public boolean isAnyStillValidReservation(List<ReservationTo> book);
}
