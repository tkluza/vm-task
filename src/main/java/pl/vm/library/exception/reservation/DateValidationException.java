package pl.vm.library.exception.reservation;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception used when date validation is not successful
 */
@SuppressWarnings("squid:S110")
public class DateValidationException extends ResponseStatusException {

    public DateValidationException() {
        super(HttpStatus.UNPROCESSABLE_ENTITY, "Given date is not correct.");
    }

    public DateValidationException(String message) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, message);
    }

    public DateValidationException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
