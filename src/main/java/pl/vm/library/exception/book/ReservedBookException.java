package pl.vm.library.exception.book;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception used when book is reserved, when it shouldn't be.
 */
@SuppressWarnings("squid:S110")
public class ReservedBookException extends ResponseStatusException {

    public ReservedBookException() {
        super(HttpStatus.UNPROCESSABLE_ENTITY, "Given book is reserved.");
    }

    public ReservedBookException(String message) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, message);
    }

    public ReservedBookException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
