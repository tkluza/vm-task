package pl.vm.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception used when operations are tried to made with wrong input parameters.
 */
@SuppressWarnings("squid:S110")
public class ParameterValidationException extends ResponseStatusException {

    private static final long serialVersionUID = -779041494072937362L;

    public ParameterValidationException() {
        super(HttpStatus.UNPROCESSABLE_ENTITY, "One of the input parameters is not correct");
    }

    public ParameterValidationException(String message) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, message);
    }

    public ParameterValidationException(HttpStatus status, String reason) {
        super(status, reason);
    }
}
