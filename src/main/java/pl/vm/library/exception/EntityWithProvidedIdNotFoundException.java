package pl.vm.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Exception used when trying to operate on Entities which don't exist in the
 * Database.
 */
@SuppressWarnings("squid:S110")
public class EntityWithProvidedIdNotFoundException extends ResponseStatusException {

    private static final long serialVersionUID = 518405525882996153L;

    public EntityWithProvidedIdNotFoundException() {
        super(HttpStatus.NOT_FOUND, "The Entity with the given ID was not found.");
    }

    public EntityWithProvidedIdNotFoundException(HttpStatus httpStatus, String message) {
        super(httpStatus, message);
    }
}
