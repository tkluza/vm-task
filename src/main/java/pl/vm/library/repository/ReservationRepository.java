package pl.vm.library.repository;

import org.springframework.data.repository.CrudRepository;
import pl.vm.library.entity.Reservation;

import java.util.List;

public interface ReservationRepository extends CrudRepository<Reservation, Long> {

    public List<Reservation> findByUserIdAndBookId(Long userId, Long bookId);

    public List<Reservation> findAllByBookId(Long bookId);
}
